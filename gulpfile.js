const { watch, task, dest, src, series } = require('gulp')
const elecConnect = require('electron-connect').server.create() 
const less = require('gulp-less')


// 样式修改使用electron-connect刷新，electron窗体
function reload (cb) {
  elecConnect.reload()
  cb()
}

// 编译less
function lessCompier () {
  return src('./*.less')
  .pipe(less())
  .pipe(dest('./'))
}

task('watch', function () {
  elecConnect.start()
  series(lessCompier)()
  watch(['./theme.less'], series(lessCompier, reload))
})