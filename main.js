const { app, BrowserWindow } = require('electron')
const client = require('electron-connect').client

// 创建electron window 
function createWindow () {
  const win = new BrowserWindow({
    webPreferences: {
      // 开启开发工具，方便调试
      devTools: true
    }
  })
  // superset web服务地址
  // 建议使用生产地址，本地就可以不需要启动superset后端服务了
  const url = 'http://127.0.0.1:5000'
  // 这个是要插入css样式文件的页面路径
  // 并不需要所有页面都插入css样式
  // 所有的看板页面都插入css样式
  const insertCssPath = new RegExp('/superset/dashboard/')
  // 如果是在本地superset的服务
  // 可直接把css文件放在superset项目的superset/static/assets目录下，然后如下使用
  // 如果是加载的生产地址，则可使用IIS或者Nginx搭建一个本地web服务，返回css文件
  // 直接如：http://localhost:8080/style/theme.css,使用
  const cssUrl = 'http://localhost:88/theme.css'

  // 加载页面
  win.loadURL(url)
  client.create(win)

  // 监听页面准备完成，插入样式
  win.webContents.on('dom-ready', () => {
    let webUrl = win.webContents.getURL()
    
    if (insertCssPath.test(webUrl)) {
      // 如果是需要插入样式文件
      // 执行js代码往当前页面插入样式文件
      win.webContents.executeJavaScript(`
        const link = document.createElement('link');
        link.setAttribute('type','text/css');
        link.setAttribute('rel','stylesheet');
        link.setAttribute('href','${cssUrl}');
        document.head.appendChild(link);`
      )

      return false
    }
    if (/\/login\/$/.test(webUrl)) {
      win.webContents.insertCSS('html, body { background-color: #f00; }')
      win.webContents.executeJavaScript(`
        $("#username")[0].value="admin";
        $("#password")[0].value="123456";
        $("form[name=login]")[0].submit();
      `, true)
      .then(() => {
        console.log('login success')
      })
      .catch(err => {
        console.log('login Error:', err)
      })
      return false
    }
    // href填入指定url
    win.webContents.executeJavaScript(`location.href="http://127.0.0.1:5000/superset/dashboard/births/"`)

  })

}

app.whenReady().then(createWindow)